/*
	A Json Web Token is made up of three parts. The first two parts are just plain text that will get encoded, but not encrypted.

		1. HEADER - contains metadata about the type of token and the cryptographic algorithms used to secure its contents. ie: 
			{
				"alg" : "HS256",
				"typ" : "JWT"
			}

		2. PAYLOAD -  is the data that we can encode into the token, any data that we want. The more data we want to encode here the bigger the JWT.
			{
				"userName": "JohnDoe",
				"isAdmin": true
			}	

		SECRET KEY - is combined with the header and the payload to create a unique hash. You are only able to verify this hash if you have the secret key.

		3. SIGNATURE - is created using the header, the payload, and the secret that is saved on the server. 
		The 'signing' algorithm takes the header, the payload, and the secret to create a unique signature.

		(HEADER + PAYLOAD) + SECRET = SIGNATURE -> JWT

		Once the server receives a JWT to grant access to a protected route, it needs to verify it in order to determine if the user really is who he claims to be.

		Once the JWT is received, the verification will take its header and payload, and together with the secret that is still saved on the server, basically create a test signature

		Example of JWT access Token:

		 "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzOTdmZjA2YmU0NmZlN2QxZDUwM2Q1MCIsImVtYWlsIjoibWVyYUBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NzE2OTgxODd9.79WnXccwzVW1WSw-jqQZFbNtE8HblkVUYJwk1nrW1kc"

		 Note: The first line of codes before the first period is for the Header. Second line of codes is for the Payload. And the third line of code is for the signature.
*/


const jwt = require("jsonwebtoken");
const secret = "EcommerceAPI";


// Creating Access Token
	module.exports.createAccessToken = (result) =>{
		// payload of the JWT
		const data = {
			id: result._id,
			email: result.email,
			isAdmin: result.isAdmin
		}
		return jwt.sign(data, secret, {});
	}


/*Token Verification*/
	module.exports.verify = (request, response, next) => {

		let token = request.headers.authorization;
		
		if(token !== undefined){
			token = token.slice(7, token.length)
			console.log(`token is ${token}`);
			
			return jwt.verify(token, secret, (error, data)=>{
				if(error){
					return response.send("Invalid Token");
				}
				else{
					next();
				}
			})
		}
		else{
			return response.send("Authentication failed! No Token provided");
		}
	}

// Token decryption

	module.exports.decode = (token) => {
		if (token === undefined){
			return null
		}
		else {
			token = token.slice(7, token.length);

			return jwt.verify(token, secret, (error, data) => {
				if (error){
					return null;
				}
				else{
					return jwt.decode(token, {complete: true}).payload
				}
			})
		}
	}


