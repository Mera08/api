const express = require("express");
const auth = require("../auth")
const userController = require("../controllers/userControllers")

const router = express.Router();

// Check Email route
router.post("/checkEmail", userController.checkEmailExists);
	
// User registration route
router.post("/register", userController.checkEmailExists, userController.registerUser);
	
// User authentication route
router.post("/login", userController.loginUser);

// Retrieving User's details route
router.get("/profile", userController.profileDetails);

// Add to cart route
router.post("/cart", auth.verify, userController.addToCart)

	
//route with params below

// Updating user's role route
router.patch("/updateRole/:userId", auth.verify, userController.updateRole)

// Orders route
router.post("/checkout/:productId", auth.verify, userController.checkout)

// Remove an item to cart
// router.delete("/removeItem/:cartId", auth.verify, userController.removeToCart)


module.exports = router;
